# Testing a ray-based setup over LAN. #

According [the ray
docs](https://docs.ray.io/en/master/package-ref.html#the-ray-command-line-api),

- `ray up CONFIG_YAML`, `ray submit CONFIG_YAML
  SCRIPT_PY` and `ray down CONFIG_YAML` are the
  preferred method to manage a cluster.
- But in practice for the _local_ cluster, it seems to
  be not working.
- A similar Makefile approach is illustrated here,
  using `ray start OPTIONS`, `ray stop`, and (manually)
  execute a command on the cluster using `ssh`

## Conclusion ##

### To start a cluster ###

```sh
make -j9 cluster
```

### To stop a cluster ###

```sh
make -j9 stop
```

### To submit/exec a task ###

```sh
ssh SERVER 'cd PARENT_DIR ; make tune'
```
Replace `SERVER` and `PARENT_DIR` in the command above
as necessary. `SERVER` is any node in the cluster,
preferably the file-system node. `PARENT_DIR` is the
location of `Makefile` that defines target `tune`.

## Abstractions ##

4 parts to be abstracted out of the training loop:
- **Model:** Tuning Depth of layers/ architecture
  search/ ablation study;
- **Data:** To study the effect of complexity of
  dataset in an ablation study;
- **Loss:** Tuning Linear combination factors, ablation
  of different loss functions; and
- **Solver:** Tuning learning rates, Speeding up
  training process etc.

