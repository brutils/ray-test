SHELL		:= /usr/bin/zsh
ENV_NAME	:= ray-test
CONDA_ROOT	:= $$HOME/miniconda3

SERVER		:= ailab1
SERVER_IP	:= 172.27.21.151
PORT		:= 9379
CLIENTS		:= ailab5 ailab6 ailab7 ailab8 ailab9 ailab0 ailab2

PARENT_DIR	:= $$HOME/code/ray_test


activate 	= source ${CONDA_ROOT}/bin/activate $1
large_random 	= $(shell echo $$RANDOM$$RANDOM$$RANDOM)

GET_PWD		:= cat ~/.ray
GET_PWD		+= | grep redis
GET_PWD		+= | cut -d= -f3
GET_PWD		+= | cut -d\' -f2
GET_PWD		+= | head -1
get-pwd		= $(and $(wildcard ~/.ray),$(shell ${GET_PWD}))

REDIS_PWD	:= $(call large_random)

SERVER_CMD	:= $(call activate,${ENV_NAME});
SERVER_CMD	+= $(and ${PARENT_DIR},cd ${PARENT_DIR};)
SERVER_CMD	+= PYTHONPATH=$$PYTHONPATH:$$HOME/code
SERVER_CMD	+= ray start --head --port ${PORT} 
SERVER_CMD	+= --dashboard-host 0.0.0.0 
SERVER_CMD	+= --redis-password "${REDIS_PWD}"

CLIENT_CMD	:= $(call activate,${ENV_NAME});
CLIENT_CMD	+= $(and ${PARENT_DIR},cd ${PARENT_DIR};)
CLIENT_CMD	+= PYTHONPATH=$$PYTHONPATH:$$HOME/code
CLIENT_CMD	+= ray start --address="${SERVER_IP}:${PORT}"
client-cmd	= ${CLIENT_CMD} $(and $(call get-pwd),--redis-password "$(call get-pwd)")

STOP_CMD	:= $(call activate,${ENV_NAME});
STOP_CMD	+= ray stop

TEST_CMD	:= $(call activate,${CONDA_ENV});
TEST_CMD	+= cd ${PARENT_DIR} ;
TEST_CMD	+= python -m ray-test

## START CLUSTER
## -----------------------------------
cluster : start-server start-clients

start-server : ~/.ray
~/.ray :
	ssh ${SERVER} '${SERVER_CMD} 2>&1 | tee ~/.ray' \
	  | tee ~/.ray

start-clients : ${CLIENTS:%=start-client-safe-%}
start-client-safe-%: start-server
	ssh $* '$(call client-cmd)'

start-clients-only : ${CLIENTS:%=start-client-only-%}
start-client-only-% :
	ssh $* '$(call client-cmd)'

## STOP CLUSTER
## -----------------------------------
stop : ${CLIENTS:%=stop-%} ${SERVER:%=stop-%}
	-ssh ${SERVER} 'rm ~/.ray'
	-rm ~/.ray

stop-% :
	ssh $* '${STOP_CMD}'

## TEST CLUSTER
## -----------------------------------
test :
	ssh ${SERVER} '${TEST_CMD}'

## CREATE ENV
## -----------------------------------
create-env :
	$(call activate,base);		\
	  conda create			\
	        -y			\
	        -n ${ENV_NAME}		\
	        python jupyter pytorch torchvision
	$(call activate,${ENV_NAME});	\
	  pip install ray 'ray[debug]' 'ray[tune]'

## REDIS PASSWORD
## -----------------------------------
get-pwd :
	@${GET_PWD}

## TUNE
## -----------------------------------
tune :
	cd ${PARENT_DIR} \
	&&	$(call activate,${ENV_NAME}) \
	&&	export PYTHONPATH=$$PYTHONPATH:$$HOME/code \
	&&	RAY=1 $(and $(call get-pwd),REDIS_PASSWORD=$(call get-pwd)) \
		  python -m $(notdir ${PARENT_DIR}).tuner.tuner_asha

## TUNE (Using Population based training)
## -----------------------------------
tune-pbt : 
	cd ${PARENT_DIR} ; $(MAKE) tensorboard

	-cd ${PARENT_DIR} \
	&&	$(call activate,${ENV_NAME}) \
	&&	export PYTHONPATH=$$PYTHONPATH:$$HOME/code \
	&&	RAY=1 $(and $(call get-pwd),REDIS_PASSWORD=$(call get-pwd)) \
		  python -m $(notdir ${PARENT_DIR}).tuner.tuner_pbt

	cd ${PARENT_DIR} ; $(MAKE) shut-tensorboard

## Tensorboard
## -----------------------------------
tensorboard : ${PARENT_DIR}/.tb
${PARENT_DIR}/.tb : ~/ray_results
	$(call activate,${ENV_NAME}) \
	&&	tensorboard --logdir ~/ray_results --bind_all \
	& 	echo $$! > $@

.PHONY : shut-tensorboard
shut-tensorboard :
	-kill -9 `cat ${PARENT_DIR}/.tb`
	-rm ${PARENT_DIR}/.tb

~/ray_results :
	[ -d $@ ] || mkdir -p $@
