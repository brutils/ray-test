import logging as lg
import shutil
from functools import partial
from tempfile import mkdtemp
import pytorch_lightning as pl
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.utilities.cloud_io import load as pl_load
from ray import tune
from ray.tune import CLIReporter
from ray.tune.schedulers import ASHAScheduler, PopulationBasedTraining
from ray.tune.integration.pytorch_lightning import TuneReportCallback, \
  TuneReportCheckpointCallback

from . mnist_model import LightningMNISTClassifier


def train_mnist_tune_checkpoint(
    config, checkpoint_dir=None,
    data_dir=None, num_epochs=10, num_gpus=0,
):
  import os

  trainer = pl.Trainer(
    max_epochs=num_epochs,
    gpus=num_gpus,
    logger=TensorBoardLogger(
      save_dir=tune.get_trial_dir(), name="", version="."),
    progress_bar_refresh_rate=0,
    callbacks=[
      TuneReportCheckpointCallback(
        metrics={
          "loss": "val_loss",
          "mean_accuracy": "val_accuracy"
        },
        filename="checkpoint",
        on="validation_end")
    ])
  if checkpoint_dir:
    # Currently, this leads to errors:
    # model = LightningMNISTClassifier.load_from_checkpoint(
    #   os.path.join(checkpoint, "checkpoint"))
    # Workaround:
    ckpt = pl_load(
      os.path.join(checkpoint_dir, "checkpoint"),
      map_location=lambda storage, loc: storage)
    model = LightningMNISTClassifier._load_model_state(ckpt, config=config)
    trainer.current_epoch = ckpt["epoch"]
  else:
    model = LightningMNISTClassifier(config=config, data_dir=data_dir)

  trainer.fit(model)

def tune_mnist_pbt(num_samples=10, num_epochs=10, gpus_per_trial=0):
  import os
  from ray.tune.logger import (
    MLFLowLogger, UnifiedLogger, DEFAULT_LOGGERS
  )

  data_dir = f'{os.getenv("HOME")}/data'
  LightningMNISTClassifier.download_data(data_dir)

  config = {
    "layer_1_size": tune.choice([32, 64, 128]),
    "layer_2_size": tune.choice([64, 128, 256]),
    "lr": 1e-3,
    "batch_size": 64,
  }

  scheduler = PopulationBasedTraining(
    time_attr="training_iteration",
    metric="loss",
    mode="min",
    perturbation_interval=4,
    hyperparam_mutations={
      "lr": lambda: tune.loguniform(1e-4, 1e-1).sample(),
      "batch_size": [32, 64, 128]
    })

  reporter = CLIReporter(
    parameter_columns=["layer_1_size", "layer_2_size", "lr", "batch_size"],
    metric_columns=["loss", "mean_accuracy", "training_iteration"])

  tune.run(
    partial(
      train_mnist_tune_checkpoint,
      data_dir=data_dir,
      num_epochs=num_epochs,
      num_gpus=gpus_per_trial),
    resources_per_trial={
      "cpu": 1,
      "gpu": gpus_per_trial
    },
    config=config,
    loggers=(UnifiedLogger,) + DEFAULT_LOGGERS,
    num_samples=num_samples,
    scheduler=scheduler,
    progress_reporter=reporter,
    name="tune_mnist_pbt")

def ray_init_may_be() :
  import os
  import ray

  kw = {}
  if os.getenv('RAY_ADDRESS') or os.getenv('RAY') :
    kw['address'] = os.getenv('RAY_ADDRESS') or 'auto'
    if os.getenv('REDIS_PASSWORD') :
      kw['_redis_password'] = os.getenv('REDIS_PASSWORD')
    
  if kw : ray.init(**kw)

if __name__ == '__main__' :
  lg.basicConfig(
    level=lg.DEBUG,
    format='%(levelname)-8s: %(message)s'
  )

  ray_init_may_be()
  tune_mnist_pbt(gpus_per_trial=1)
