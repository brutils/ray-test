import shutil
from functools import partial
from tempfile import mkdtemp
import pytorch_lightning as pl
# from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.utilities.cloud_io import load as pl_load
from ray import tune
from ray.tune import CLIReporter
from ray.tune.schedulers import ASHAScheduler, PopulationBasedTraining
from ray.tune.integration.pytorch_lightning import TuneReportCallback, \
  TuneReportCheckpointCallback

from . mnist_model import LightningMNISTClassifier

from ray.tune.integration.pytorch_lightning import TuneReportCallback
# callback = TuneReportCallback({
#   "loss": "avg_val_loss",
#   "mean_accuracy": "avg_val_accuracy"
# }, on="validation_end")

import logging as lg

def train_mnist_tune(config, data_dir=None, num_epochs=10, num_gpus=0):
  import os
  # from pytorch_lightning.loggers import MLFlowLogger
  from pytorch_lightning.loggers import TensorBoardLogger

  model = LightningMNISTClassifier(config, data_dir)
  trainer = pl.Trainer(
    max_epochs=num_epochs,
    gpus=num_gpus,
    logger=TensorBoardLogger(
      save_dir=tune.get_trial_dir(), name="", version="."),
    # logger = MLFlowLogger(
    #   experiment_name="MNIST Tune",
    #   tracking_uri=f"file:{os.getenv('HOME')}/ray_results/ml-runs",
    # ),
    progress_bar_refresh_rate=0,
    callbacks=[
      TuneReportCallback(
        {
          "loss": "val_loss",
          "mean_accuracy": "val_accuracy"
        },
        on="validation_end")
    ])

  trainer.fit(model)

def tune_mnist_asha(num_samples=10, num_epochs=10, gpus_per_trial=0):
  import os
  from ray.tune.logger import (
    MLFLowLogger, UnifiedLogger, DEFAULT_LOGGERS
  )

  data_dir = f'{os.getenv("HOME")}/data'
  LightningMNISTClassifier.download_data(data_dir)

  config = {
    "layer_1_size": tune.choice([32, 64, 128]),
    "layer_2_size": tune.choice([64, 128, 256]),
    "lr": tune.loguniform(1e-4, 1e-1),
    "batch_size": tune.choice([32, 64, 128]),
  }

  scheduler = ASHAScheduler(
    metric="loss",
    mode="min",
    max_t=num_epochs,
    grace_period=1,
    reduction_factor=2)

  reporter = CLIReporter(
    parameter_columns=["layer_1_size", "layer_2_size", "lr", "batch_size"],
    metric_columns=["loss", "mean_accuracy", "training_iteration"])

  # lg.info(f'Tune get_trial_dir(): {tune.get_trial_dir()}')

  tune.run(
    partial(
      train_mnist_tune,
      data_dir=data_dir,
      num_epochs=num_epochs,
      num_gpus=gpus_per_trial),
    resources_per_trial={
      "cpu": 1,
      "gpu": gpus_per_trial
    },
    loggers=(UnifiedLogger,) + DEFAULT_LOGGERS,
    log_to_file=True,
    config=config,
    num_samples=num_samples,
    scheduler=scheduler,
    progress_reporter=reporter,
    name="tune_mnist_asha")

def ray_init_may_be() :
  import os
  import ray

  kw = {}
  if os.getenv('RAY_ADDRESS') or os.getenv('RAY') :
    kw['address'] = os.getenv('RAY_ADDRESS') or 'auto'
    if os.getenv('REDIS_PASSWORD') :
      kw['_redis_password'] = os.getenv('REDIS_PASSWORD')
    
  if kw : ray.init(**kw)

if __name__ == '__main__' :
  lg.basicConfig(
    level=lg.DEBUG,
    format='%(levelname)-8s: %(message)s'
  )

  ray_init_may_be()
  tune_mnist_asha(gpus_per_trial=1)
