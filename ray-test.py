import ray

@ray.remote
def f():
  import time

  time.sleep(0.01)
  return ray.services.get_node_ip_address()

if __name__ == '__main__' :

  # initialize ray
  ray.init(address="auto")

  # Get a list of the IP addresses of the nodes that have
  # joined the cluster.
  nodes = set(ray.get([f.remote() for _ in range(1000)]))
  print (nodes)
